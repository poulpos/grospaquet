#' sum
#'
#' @param a first number
#' @param b second number
#' @return the product
#' @export
somme <- function(a,b) {
  return(a + b)
}



#' Test dependency with other file
#'
#' @param a first number
#' @param b second number
#' @param c second number
#'
#' @return a * b + c
#' @export
sopro <- function(a,b,c) {
  return(somme(mult(a,b), c))
}

#' test dependency with global
#'
#' @param a first number
#' @return a * MY_GLOBAL
#' @export
soproglo <- function(a) {
  return(mult(a, MY_GLOBAL))
}

#' test dependency with foreign
#'
#' @return dict
#' @export
soproglo <- function(a) {
  return(fromJSON(file="/home/poulpos"))
}
